var express = require('express')
var app = express()

const numberWithCommas = x => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')

app.get('/', function (req, res) {
  res.send('Hello World!')
})

app.get('/benchmark', function (req, res) {

  const now = new Date().getTime()
  const max = 50000000

  for (var i = 0; i < max; i++) {
    Math.pow(Math.sqrt(i), Math.sqrt(i))
  }

  const done = new Date().getTime()

  res.send(`${numberWithCommas(max)} loops took ${(done - now) / 1000} seconds to complete`)
})

app.get('/ddos', function (req, res) {
  res.send("You're trying to stress me :/")
})

app.listen(8888, function () {
  console.log('App listening on port 8888!')
})